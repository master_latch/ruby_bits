#!/usr/bin/ruby -w

[ 'cat', 'dog', 'horse' ].each do |animal|
    print animal, " -- "
end

print "\n"
5.times { |i| print "* #{i}" }
print "\n"
3.upto(6) {|i| print i }
print "\n"
('a'..'e').each{|char| print char }
print "\n"

