#!/usr/bin/ruby -w

class Account
    attr_reader :balance
    attr_writer :balance

#    protected :balance

    def initialize(balance)
        @balance = balance
    end

    def greaterBalanceThan(other)
        return @balance > other.balance
    end
end

class Accounts

#    protected :balance
#
#    def greaterBalanceThan(other)
#        return @balance > other.balance
#    end
#

    def initialize(checkings, savings)
        @checkings = Account.new(checkings)
        @savings = Account.new(savings)
    end

    private

    def debit(account, amount)
        account.balance -= amount
    end
    def credit(account, amount)
        account.balance += amount
    end

    public

    def transferToSavings(amount)
        debit(@checkings, amount)
        credit(@savings, amount)
    end
end

a = Accounts.new(10, 20)
puts a.inspect
a.transferToSavings(5)
puts a.inspect
