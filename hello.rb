#!/usr/bin/ruby -w

class Logger
    private_class_method :new
    @@logger = nil
    def Logger.create
        @@logger = new unless @@logger
        @@logger
    end
end

class Song
    @@plays = 0
    def Song.plays
        @@plays
    end
    attr_reader :name, :artist, :duration
    attr_writer :duration
    def initialize(name, artist, duration)
        @name     = name
        @artist   = artist
        @duration = duration
        @plays    = 0
    end
    def play
        @plays += 1
        @@plays += 1
    end
    def to_s
        "Song: #{@name}--#{@artist} (#{@duration})"
    end
end

class KaraokeSong < Song
    def initialize(name, artist, duration, lyrics)
        super(name, artist, duration)
        @lyrics = lyrics
    end
    def to_s
        super + " [#{@lyrics}]"
    end
end

aSong = Song.new("Bicylops", "Fleck", 260)
puts aSong.artist
aSong.play

bSong = KaraokeSong.new("My Way", "Sinatra", 255, "And now, the...")
puts bSong.to_s
bSong.play

puts Song.plays
